package sk.allas.gamestates;

import java.awt.Graphics2D;

/**
 *
 * @author Allas
 */
public abstract class GameState {
    protected GameStateManager gsm;

    public abstract void initialize();
    public abstract void update();
    public abstract void draw(Graphics2D g2d);
    public abstract void keyPressed(int key);
    public abstract void keyReleased(int key);
}
