package sk.allas.gamestates;

import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author Allas
 */
public class GameStateManager {
    public static final int MENU = 0;
    public static final int CAVE1 = 1;
    public static final int CAVE2 = 2;
    public static final int WIN = 3;


    private ArrayList<GameState> gameStates;
    private int currentState;



    public GameStateManager() {
        gameStates = new ArrayList<GameState>();

        currentState = MENU;
        gameStates.add(new MenuState(this));
        gameStates.add(new Cave1State(this));
        gameStates.add(new Cave2State(this));
        gameStates.add(new WinState(this));

    }

    public void setState(int state) {
        currentState = state;
        gameStates.get(currentState).initialize();
    }

    public void update() {
        gameStates.get(currentState).update();
    }

    public void draw(Graphics2D g2d) {
        gameStates.get(currentState).draw(g2d);
    }

    public void keyPressed(int key) {
        gameStates.get(currentState).keyPressed(key);
    }

    public void keyReleased(int key) {
        gameStates.get(currentState).keyReleased(key);
    }

    public void resetLevels() {
        gameStates.set(CAVE1, new Cave1State(this));
        gameStates.set(CAVE2, new Cave2State(this));
    }
}
