package sk.allas.gamestates;

import sk.allas.gameobject.Dumbeetle;
import sk.allas.gameobject.Enemy;
import sk.allas.gameobject.Player;
import sk.allas.map.Background;
import sk.allas.map.HUD;
import sk.allas.map.LevelMap;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public class Cave1State extends GameState{
    private LevelMap map;

    private Background background;
    private HUD hud;

    private Player player;
    private ArrayList<Enemy> enemies;


    public Cave1State(GameStateManager gsm) {
        this.gsm = gsm;
        enemies = new ArrayList<Enemy>();
        background = new Background("/cavebackground.png", 0);
        hud = new HUD();
    }

    @Override
    public void initialize() {
        map = new LevelMap(100, "/cavetileset.png", "/cave1.txt");
        map.setPosition(0, 0);

        player = new Player(map);
        player.initialize();
        player.setPosition(300, 300);
        player.setEnemies(enemies);

        Enemy enemy = new Dumbeetle(map, player);
        enemy.initialize();
        enemy.setPosition(1600, 400);
        enemies.add(enemy);

        enemy = new Dumbeetle(map, player);
        enemy.initialize();
        enemy.setPosition(290, 950);
        enemies.add(enemy);

        enemy = new Dumbeetle(map, player);
        enemy.initialize();
        enemy.setPosition(1750, 1200);
        enemies.add(enemy);

        enemy = new Dumbeetle(map, player);
        enemy.initialize();
        enemy.setPosition(1502, 899);
        enemies.add(enemy);

        enemy = new Dumbeetle(map, player);
        enemy.initialize();
        enemy.setPosition(1170, 999);
        enemies.add(enemy);

        enemy = new Dumbeetle(map, player);
        enemy.initialize();
        enemy.setPosition(2649, 499);
        enemies.add(enemy);
    }

    @Override
    public void update() {
        if (map != null) {
            map.update();
        }
        if (player != null) {
            player.update();
            if (player.isOnRightSideOfLevel()) {
                gsm.setState(GameStateManager.CAVE2);
            }
        }

        for(Enemy e : enemies) {
            e.update();
        }
    }

    @Override
    public void draw(Graphics2D g2d) {
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);

        if (background != null) {
            background.draw(g2d);
        }

        if (map != null) {
            map.draw(g2d);
        }
        if (player != null) {
            player.draw(g2d);
            hud.draw(g2d, player.getHitPoints());
        }

        for(Enemy e : enemies) {
            e.draw(g2d);
        }
    }

    @Override
    public void keyPressed(int key) {
        player.keyPressed(key);
        if (!player.isAlive()) {
            if (key == KeyEvent.VK_ESCAPE) {
                gsm.setState(GameStateManager.MENU);
                gsm.resetLevels();
            }
        }
    }

    @Override
    public void keyReleased(int key) {
        player.keyReleased(key);
    }

    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }
}
