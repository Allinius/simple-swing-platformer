package sk.allas.gamestates;

import sk.allas.map.Background;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public class MenuState extends GameState{
    private Background background;

    private Font font;

    private BufferedImage logo;

    private int currentChoice = 0;
    private String[] options = {"Start", "Quit"};



    public MenuState(GameStateManager gsm) {
        this.gsm = gsm;

        background = new Background("/menubackground.png", -0.5);
        try {
            logo = ImageIO.read(getClass().getResourceAsStream("/logo.png"));
        } catch (IOException ex) {
            Logger.getLogger(MenuState.class.getName()).log(Level.SEVERE, null, ex);
        }

        font = new Font("Arial", Font.PLAIN, 20);
    }

    @Override
    public void initialize() {

    }

    @Override
    public void update() {
        background.update();
    }

    @Override
    public void draw(Graphics2D g2d) {
        background.draw(g2d);

        g2d.drawImage(logo, (Game.WIDTH - logo.getWidth()) / 2, 50, null);

        g2d.setFont(font);

        for (int i = 0; i < options.length; i++) {
            if (i == currentChoice) {
                g2d.setColor(Color.CYAN.brighter());
            } else {
                g2d.setColor(Color.CYAN.darker());
            }
            g2d.drawString(options[i], 50, 350 + (i*20));
        }
    }

    private void select() {
        if (currentChoice == 0) {
            gsm.setState(GameStateManager.CAVE1);
        }
        if (currentChoice == 1) {
            System.exit(0);
        }
    }

    @Override
    public void keyPressed(int key) {
        if(key == KeyEvent.VK_ENTER) {
            select();
        }
        if(key == KeyEvent.VK_UP) {
            currentChoice--;
            if (currentChoice == -1) {
                currentChoice = options.length -1;
            }
        }
        if(key == KeyEvent.VK_DOWN) {
            currentChoice++;
            if (currentChoice == options.length) {
                currentChoice = 0;
            }
        }
    }

    @Override
    public void keyReleased(int key) {

    }

}
