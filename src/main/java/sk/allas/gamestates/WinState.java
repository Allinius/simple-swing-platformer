package sk.allas.gamestates;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public class WinState extends GameState{

    private Font font;

    WinState(GameStateManager gsm) {
        this.gsm = gsm;
        font = new Font("Arial", Font.PLAIN, 40);
    }

    @Override
    public void initialize() {

    }

    @Override
    public void update() {

    }

    @Override
    public void draw(Graphics2D g2d) {
        g2d.setFont(font);
        g2d.setColor(Color.CYAN.brighter());

        g2d.drawString("You Win", Game.WIDTH/2 - 100, 100);
        g2d.drawString("press esc to return to menu", Game.WIDTH/2 - 200, 200);
    }

    @Override
    public void keyPressed(int key) {

    }

    @Override
    public void keyReleased(int key) {
        if (key == KeyEvent.VK_ESCAPE) {
            gsm.setState(GameStateManager.MENU);
            gsm.resetLevels();
        }
    }

}
