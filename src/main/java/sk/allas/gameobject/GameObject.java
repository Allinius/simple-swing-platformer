package sk.allas.gameobject;

import sk.allas.map.LevelMap;
import sk.allas.map.Tile;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public abstract class GameObject {
    protected LevelMap levelMap;

    protected double xMapPosition;
    protected double yMapPosition;
    protected double xScreenPosition;
    protected double yScreenPosition;

    protected double xVel;
    protected double yVel;

    protected AnimationSet animationSet;
    protected int spriteWidth;
    protected int spriteHeight;

    protected boolean facingRight;
    protected boolean walking;
    protected boolean airborne;
    protected boolean attacking;
    protected boolean jumping;

    protected int boundingBoxWidth;
    protected int boundingBoxHeight;
    protected boolean topRightSolid;
    protected boolean topLeftSolid;
    protected boolean bottomRightSolid;
    protected boolean bottomLeftSolid;

    protected boolean onRightSideOfLevel = false;

    public boolean isOnRightSideOfLevel() {
        return onRightSideOfLevel;
    }

    public GameObject(LevelMap map) {
        levelMap = map;
        animationSet = new AnimationSet();
    }

    public void setPosition(double x, double y) {
        xMapPosition = x;
        yMapPosition = y;
        xScreenPosition = xMapPosition - levelMap.getxPos() + (Game.WIDTH / 2);
        yScreenPosition = yMapPosition - levelMap.getyPos() + (Game.HEIGHT / 2);
    }

    public void setVelocity(double xVel, double yVel) {
        this.xVel = xVel;
        this.yVel = yVel;
    }

    public double getxMapPosition() {
        return xMapPosition;
    }

    public double getyMapPosition() {
        return yMapPosition;
    }


    protected void calculateCollision(double x, double y){
        int rightSideColumn = (int)(x + boundingBoxWidth / 2) / levelMap.getTileSize();
        int leftSideColumn = (int)(x - boundingBoxWidth / 2) / levelMap.getTileSize();
        int topSideRow = (int)(y - boundingBoxHeight) / levelMap.getTileSize();
        int bottomSideRow = (int)(y) / levelMap.getTileSize();

        if (rightSideColumn == levelMap.getWidth() / levelMap.getTileSize() -1) {
            onRightSideOfLevel = true;
        }

        topRightSolid = levelMap.getType(rightSideColumn, topSideRow) == Tile.SOLID;
        topLeftSolid = levelMap.getType(leftSideColumn, topSideRow) == Tile.SOLID;
        bottomRightSolid = levelMap.getType(rightSideColumn, bottomSideRow) == Tile.SOLID;
        bottomLeftSolid = levelMap.getType(leftSideColumn, bottomSideRow) == Tile.SOLID;
    }
    public void updatePosition() {
        double xDest = xMapPosition + xVel;
        double yDest = yMapPosition + yVel;
        double xTemp = xMapPosition;
        double yTemp = yMapPosition;

        calculateCollision(xDest, yMapPosition);

        if (xVel > 0) {
            if (topRightSolid || bottomRightSolid) {
                xVel = 0;
                xTemp = ((int)(xMapPosition / levelMap.getTileSize()) + 1) * levelMap.getTileSize() - boundingBoxWidth/2 - 1;
            }
        }
        if (xVel < 0) {
            if (topLeftSolid || bottomLeftSolid) {
                xVel = 0;
                xTemp = ((int)(xMapPosition / levelMap.getTileSize())) * levelMap.getTileSize() + boundingBoxWidth/2;
            }
        }

        calculateCollision(xMapPosition, yDest);

        if (yVel > 0) {
            if (bottomLeftSolid || bottomRightSolid) {
                yVel = 0;
                yTemp = ((int)((yMapPosition) / levelMap.getTileSize()) + 1) * levelMap.getTileSize() - 1;
                airborne = false;
            }
        }
        if (yVel < 0){
            if (topLeftSolid || topRightSolid) {
                yVel = 0;
                yTemp = ((int)((yMapPosition) / levelMap.getTileSize())) * levelMap.getTileSize() + boundingBoxHeight;
                jumping = false;
            }
        }
        xMapPosition = xTemp + xVel;
        yMapPosition = yTemp + yVel;
    }


    public abstract void initialize();
    public abstract void update();

    public void draw(Graphics2D g2d) {
        if (animationSet != null && animationSet.isReady()) {
            BufferedImage tempImage = animationSet.getFrame();
            if (facingRight) {
                g2d.drawImage(tempImage, (int)xScreenPosition - spriteWidth/2, (int)yScreenPosition - spriteHeight, null);
            } else {
                g2d.drawImage(tempImage, (int)xScreenPosition + spriteWidth/2, (int)yScreenPosition - spriteHeight, -spriteWidth, spriteHeight, null);
            }
        }
    }

    public Rectangle getBoundingBox() {
        return new Rectangle((int)(xMapPosition - boundingBoxWidth/2), (int)(yMapPosition - boundingBoxHeight), (int)boundingBoxWidth, (int)boundingBoxHeight);
    }

    public boolean intersects(GameObject other) {
        return this.getBoundingBox().intersects(other.getBoundingBox());
    }

}
