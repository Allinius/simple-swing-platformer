package sk.allas.gameobject;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author Allas
 */
class AnimationSet {
    private ArrayList<Animation> animations;

    private int currentAnimation;

    private int currentFrame;

    private long delay;
    private long lastUpdated;

    private boolean runOnce;
    private boolean looping = true;

    public AnimationSet() {
        animations = new ArrayList<Animation>();
    }


    public void loadAnimation(String spriteSheet, int tileWidth, int tileHeight, int row, int count) {
        Animation anim = new Animation();
        anim.loadFrames(spriteSheet, tileWidth, tileHeight, row, count);
        animations.add(anim);

        if (animations.size() == 1) {
            currentAnimation = 0;
            currentFrame = 0;
            runOnce = false;
            lastUpdated = System.currentTimeMillis();
        }
    }

    public void changeAnimation(int animNum) {
        if (animNum < animations.size()) {
            currentAnimation = animNum;
            currentFrame = 0;
            runOnce = false;
            lastUpdated = System.currentTimeMillis();
        }
    }
    public BufferedImage getFrame() {
        return animations.get(currentAnimation).getFrame(currentFrame);
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public boolean hasRunOnce() {
        return runOnce;
    }

    public boolean isReady() {
        if (animations.size() > 0) return true;
        return false;
    }

    public int getCurrentAnimation() {
        return currentAnimation;
    }

    public void update() {
        if (animations.size() > 0) {
            long currentTime = System.currentTimeMillis();

            if (currentTime - lastUpdated > delay) {
                if(currentFrame < animations.get(currentAnimation).getFrameCount() - 1) {
                    if(looping || !runOnce){
                        currentFrame += 1;
                    }

                } else {
                    if (looping) {
                       currentFrame = 0;
                    }
                    runOnce = true;
                }
                lastUpdated = currentTime;
            }
        }
    }

    public boolean isLooping() {
        return looping;
    }

    public void setLooping(boolean looping) {
        this.looping = looping;
    }

}
