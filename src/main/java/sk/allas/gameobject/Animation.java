package sk.allas.gameobject;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Allas
 */
class Animation {
    private ArrayList<BufferedImage> frames;

    private int frameCount;

    public Animation() {
        frames = new ArrayList<BufferedImage>();
    }

    public void loadFrames(String spriteSheetPath, int tileWidth, int tileHeight, int row, int count) {
        try {
            BufferedImage spriteSheet = ImageIO.read(getClass().getResourceAsStream(spriteSheetPath));
            for (int i = 0; i < count; i++) {
                BufferedImage subimage = spriteSheet.getSubimage(tileWidth * i, tileHeight * row, tileWidth, tileHeight);
                frames.add(subimage);
            }
            frameCount = count;
        } catch (IOException ex) {
            Logger.getLogger(Animation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public BufferedImage getFrame(int num){
        return frames.get(num);
    }

    public int getFrameCount() {
        return frameCount;
    }
}
