package sk.allas.gameobject;

import sk.allas.map.LevelMap;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public class Player extends GameObject{

    private int currentAnimation;
    private static final int IDLE = 0;
    private static final int WALKING = 1;
    private static final int JUMPING = 2;
    private static final int ATTACKING = 3;
    private static final int DEAD = 4;

    private boolean jumpPressed = false;
    private boolean rightPressed = false;
    private boolean leftPressed = false;
    private boolean attackPressed = false;

    private long lastJumped = 0;
    private long lastDamaged = 0;
    private boolean jumpReady = true;
    private boolean attackReady = true;
    private boolean attacking = false;

    private static double MAXMOVEVELOCITY = 7;
    private static double MOVEINCREMENT = 1;
    private static long JUMPDELAY = 250;
    private static double JUMPINCREMENT = 15;
    private static long HITINVINCIBILITYDELAY = 1000;
    private static int HITRANGE = 50;


    private int hitPoints;
    private boolean alive;
    private ArrayList<Enemy> enemies;

    public Player(LevelMap map) {
        super(map);
    }

    public void initialize() {
        alive = true;
        hitPoints = 3;

        currentAnimation = IDLE;
        spriteWidth = 60;
        spriteHeight = 80;
        boundingBoxWidth = 40;
        boundingBoxHeight = 60;

        animationSet = new AnimationSet();
        animationSet.setDelay(100);
        animationSet.loadAnimation("/playertemp.png", spriteWidth, spriteHeight, 0, 2);
        animationSet.changeAnimation(currentAnimation);
        animationSet.loadAnimation("/playertemp.png", spriteWidth, spriteHeight, 1, 4);
        animationSet.loadAnimation("/playertemp.png", spriteWidth, spriteHeight, 2, 3);
        animationSet.loadAnimation("/playertemp.png", spriteWidth, spriteHeight, 3, 3);
        animationSet.loadAnimation("/playertemp.png", spriteWidth, spriteHeight, 4, 4);
    }

    private void manageInputs() {
        if (alive) {
            if (rightPressed) {
                xVel += MOVEINCREMENT;
                walking = true;
                facingRight = true;
            }
            if (leftPressed) {
                xVel -= MOVEINCREMENT;
                walking = true;
                facingRight = false;
            }
            if (jumpPressed) {
                long currentTime = System.currentTimeMillis();
                if (!airborne && jumpReady) {
                    yVel = -JUMPINCREMENT;
                    lastJumped = currentTime;
                    airborne = true;
                    jumping = true;
                    jumpReady = false;
                }
                if (currentTime - lastJumped < JUMPDELAY && jumping) {
                    yVel = -JUMPINCREMENT;
                }
            }
            if (attackPressed && attackReady) {
                attacking = true;
            }
        }
        if (!rightPressed && !leftPressed) {
            if(Math.abs(xVel) < 2) {
                xVel = 0;
            }
            if (xVel > 0) {
                xVel -= MOVEINCREMENT;
            }
            if (xVel < 0) {
                xVel += MOVEINCREMENT;
            }
            walking = false;
        }
        if (Math.abs(xVel) > MAXMOVEVELOCITY) {
            if (xVel > 0) {
                xVel = MAXMOVEVELOCITY;
            } else {
                xVel = -MAXMOVEVELOCITY;
            }
        }
        yVel = yVel + 2;
    }

    private void updateCurrentAnimation() {
        if (animationSet.hasRunOnce() && currentAnimation == ATTACKING) {
            attacking = false;
            attackReady = true;
            System.out.println("hu");
        }

        if (!alive) {
            currentAnimation = DEAD;
        } else if (attacking) {
            currentAnimation = ATTACKING;
        } else if (airborne) {
            currentAnimation = JUMPING;
        } else if (walking) {
            currentAnimation = WALKING;
        } else {
            currentAnimation = IDLE;
        }

        if (animationSet.getCurrentAnimation() != currentAnimation) {
            animationSet.changeAnimation(currentAnimation);
            if(currentAnimation == DEAD) {
                animationSet.setLooping(false);
            }
        }
    }

    @Override
    public void update() {
        manageInputs();
        updateCurrentAnimation();
        if (levelMap != null) {
            updatePosition();
        }

       setPosition(xMapPosition, yMapPosition);
        if (animationSet != null) {
            animationSet.update();
        }
        if (enemies != null) {
            checkEnemies();
        }
        System.out.println(xMapPosition + " " + yMapPosition);
    }

    @Override
    public void draw(Graphics2D g2d) {
        super.draw(g2d);
    }

    public void keyPressed(int key) {
        if (key == KeyEvent.VK_Z) {
            jumpPressed = true;
        } else if (key == KeyEvent.VK_RIGHT) {
            leftPressed = false;
            rightPressed = true;
        }
        else if (key == KeyEvent.VK_LEFT) {
            rightPressed = false;
            leftPressed = true;
        } else if (key == KeyEvent.VK_X) {
            attackPressed = true;
        }
        if (key == KeyEvent.VK_S) {
            System.out.println(topLeftSolid + " " + topRightSolid + " " + bottomLeftSolid + " " + bottomRightSolid);
        }
    }

    public void keyReleased(int key) {
        if (key == KeyEvent.VK_Z) {
            jumpPressed = false;
            jumpReady = true;
        }
        else if (key == KeyEvent.VK_RIGHT) {
            rightPressed = false;
        }
        else if (key == KeyEvent.VK_LEFT) {
            leftPressed = false;
        } else if (key == KeyEvent.VK_X) {
            attackPressed = false;
        }
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void damageHitPoints(int d) {
        long currentTime = System.currentTimeMillis();

        if (currentTime - lastDamaged > HITINVINCIBILITYDELAY) {
            hitPoints -= d;
            if (hitPoints <= 0) {
                alive = false;
                attacking = false;
            }
            lastDamaged = currentTime;
            if(facingRight) {
                xVel = -15;
            } else {
                xVel = 15;
            }
            yVel = -15;
        }
    }

    public boolean isAlive() {
        return alive;
    }

    @Override
    public void setPosition(double x, double y) {
        super.setPosition(x, y);
        levelMap.setDestination((int)xMapPosition, (int)yMapPosition);
    }

    public void setEnemies(ArrayList<Enemy> enemies) {
        this.enemies = enemies;
    }

    public void checkEnemies() {
        for (Enemy enemy : enemies) {
            if (enemy.isAlive()) {
                if (this.intersects(enemy)) {
                    this.damageHitPoints(enemy.getContactDamage());
                }
            }
            if(attacking) {
                Rectangle attackBox;
                if (facingRight) {
                    attackBox = new Rectangle((int)xMapPosition, (int)(yMapPosition - boundingBoxHeight), HITRANGE,(int)boundingBoxHeight);
                } else {
                    attackBox = new Rectangle((int)xMapPosition - HITRANGE, (int)(yMapPosition - boundingBoxHeight), HITRANGE,(int)boundingBoxHeight);
                }
                if (attackBox.intersects(enemy.getBoundingBox())) {
                    enemy.damageHitPoints(1, facingRight);
                }
            }
        }
    }
}
