package sk.allas.gameobject;

import sk.allas.map.LevelMap;
import java.awt.Graphics2D;
import java.util.Random;

/**
 *
 * @author Allas
 */
public class Dumbeetle extends Enemy {

    Random rand;

    private int currentAnimation;
    private static final int WALKING = 0;
    private static final int DEAD = 1;

    public Dumbeetle(LevelMap map, Player player) {
        super(map, player);
        rand = new Random();
    }

    @Override
    public void initialize() {
        MAXMOVEVELOCITY = 4;
        MOVEINCREMENT = 1;
        JUMPDELAY = 250;
        JUMPINCREMENT = 30;

        alive = true;
        hitPoints = 2;
        contactDamage = 1;

        spriteWidth = 50;
        spriteHeight = 50;
        boundingBoxWidth = 40;
        boundingBoxHeight = 40;

        animationSet = new AnimationSet();
        animationSet.setDelay(250);
        animationSet.loadAnimation("/dumbeetle.png", spriteWidth, spriteHeight, 0, 3);
        animationSet.changeAnimation(WALKING);
        animationSet.loadAnimation("/dumbeetle.png", spriteWidth, spriteHeight, 1, 3);
    }

    @Override
    public void update() {
        thinkWhatToDo();
        updateCurrentAnimation();
        if (levelMap != null) {
            updatePosition();
        }
        setPosition(xMapPosition, yMapPosition);

        if (animationSet != null) {
            animationSet.update();
        }
    }


    private void updateCurrentAnimation() {
        if (!alive) {
            currentAnimation = DEAD;
            animationSet.setLooping(false);
        } else if (walking) {
            currentAnimation = WALKING;
        }
        if (animationSet.getCurrentAnimation() != currentAnimation) {
            animationSet.changeAnimation(currentAnimation);
        }
    }

    private void thinkWhatToDo() {
        if (alive) {
            if (xVel != 0 && !airborne) {
                calculateCollision(xMapPosition + xVel, yMapPosition + 1);
                if (!bottomLeftSolid && !bottomRightSolid) {
                    xVel = -xVel;
                }
            }
            if (facingRight && xVel != 0) {
                xVel += MOVEINCREMENT;
                walking = true;
                facingRight = true;
            } else if (!facingRight && xVel != 0) {
                xVel -= MOVEINCREMENT;
                walking = true;
                facingRight = false;
            }

            if ((xVel == 0 && !facingRight && !airborne)) {
                xVel += MOVEINCREMENT;
                walking = true;
                facingRight = true;
            } else if ((xVel == 0 && facingRight && !airborne)) {
                xVel -= MOVEINCREMENT;
                walking = true;
                facingRight = false;
            }
        } else {
            if(Math.abs(xVel) < 2) {
                xVel = 0;
            }
            if (xVel > 0) {
                xVel -= MOVEINCREMENT;
            }
            if (xVel < 0) {
                xVel += MOVEINCREMENT;
            }
            walking = false;
        }

        if (Math.abs(xVel) > MAXMOVEVELOCITY) {
            if (xVel > 0) {
                xVel = MAXMOVEVELOCITY;
            } else {
                xVel = -MAXMOVEVELOCITY;
            }
        }
        yVel = yVel + 2;
    }
}
