package sk.allas.gameobject;

import sk.allas.map.LevelMap;

/**
 *
 * @author Allas
 */
public abstract class Enemy extends GameObject{

    protected int currentState;
    protected static final int CALM = 0;
    protected static final int AGGROED = 1;

    protected static double MAXMOVEVELOCITY;
    protected static double MOVEINCREMENT;
    protected static long JUMPDELAY;
    protected static double JUMPINCREMENT;
    protected long lastJumped;
    protected boolean jumpReady = true;

    private static long HITINVINCIBILITYDELAY = 1000;
    private long lastDamaged = 0;

    protected Player player;

    protected int hitPoints;
    protected boolean alive;
    protected int contactDamage;

    public Enemy(LevelMap map, Player player) {
        super(map);
        this.player = player;
    }

    public int getContactDamage() {
        return contactDamage;
    }

    public void damageHitPoints(int d, boolean attackerFacingRight) {
        long currentTime = System.currentTimeMillis();

        if (currentTime - lastDamaged > HITINVINCIBILITYDELAY) {
            hitPoints -= d;
            if (hitPoints <= 0) {
                alive = false;
            }
            lastDamaged = currentTime;
            if(!attackerFacingRight) {
                xVel = -15;
            } else {
                xVel = 15;
            }
            yVel = -15;
        }
    }

    public boolean isAlive() {
        return alive;
    }

}
