package sk.allas.where;

import sk.allas.gamestates.*;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Allas
 */
public class Game extends JPanel implements KeyListener, Runnable{
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private Thread animThread;
    private boolean running;
    private int FPS = 60;
    private long targetTime = 1000/FPS;

    private BufferedImage image;
    private Graphics2D g2d;

    private GameStateManager gsm;



    public Game(){
        //super();
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setFocusable(true);
        requestFocus();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (animThread == null) {
            addKeyListener(this);
            animThread = new Thread(this);
            animThread.start();
        }
    }

    private void initialize() {
         image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
         g2d = (Graphics2D) image.getGraphics();
         g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

         running = true;

         gsm = new GameStateManager();
    }

    @Override
    public void run() {
        initialize();

        long start;
        long elapsed;
        long wait;

        while (running) {
            start = System.currentTimeMillis();

            update();
            draw();
            drawToScreen();

            elapsed = System.currentTimeMillis() - start;
            wait = targetTime - elapsed;

            if (wait < 0) {
                wait = 2;
            }

            try {
                Thread.sleep(wait);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void update() {
        gsm.update();
    }

    private void draw() {
        gsm.draw(g2d);
    }

    private void drawToScreen() {
        Graphics g = getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
    }

    @Override
    public void keyTyped(KeyEvent key) {}

    @Override
    public void keyPressed(KeyEvent key) {
        gsm.keyPressed(key.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent key) {
        gsm.keyReleased(key.getKeyCode());
    }
}
