package sk.allas.where;

import javax.swing.JFrame;

/**
 *
 * @author Allas
 */
public class Where {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame gameFrame = new JFrame("Where?");
        gameFrame.setContentPane(new Game());
        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameFrame.setResizable(false);
        gameFrame.pack();
        gameFrame.setVisible(true);
    }
}
