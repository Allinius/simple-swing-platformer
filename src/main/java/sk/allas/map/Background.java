package sk.allas.map;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public class Background {
    private BufferedImage image;
    private int width;
    private int height;

    private double x;
    private double y;
    private double dx;


    public Background(String s, double dx) {
        try {
            image = ImageIO.read(getClass().getResourceAsStream(s)); //TODO
            width = image.getWidth();
            height = image.getHeight();
            this.dx = dx;
        } catch (IOException ex) {
            Logger.getLogger(Background.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setPosition(double x, double y) {
        this.x = x;
        this.y = y;
        fixPosition();
    }

    public void setVector(double dx) {
        this.dx = dx;
    }

    public void update() {
        x += dx;
        fixPosition();
    }

    public void draw(Graphics2D g2d) {
        g2d.drawImage(image, (int)x, (int)y, null);

        if(x < 0) {
            double tempX = x + width;
            while (tempX < Game.WIDTH) {
                g2d.drawImage(image, (int)tempX, (int)y, null);
                tempX += width;
            }
        } else {
            double tempX = x + width;
            while (tempX < Game.WIDTH) {
                g2d.drawImage(image, (int)tempX, (int)y, null);
                tempX += width;
            }
            tempX = x - width;
            while (tempX > -width) {
                g2d.drawImage(image, (int)tempX, (int)y, null);
                tempX -= width;
            }
        }
    }

    private void fixPosition() {
        while(x <= -width) x += width;
        while(x >= width) x -= width;
    }
}
