package sk.allas.map;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public class HUD {
    private BufferedImage heart;

    public HUD() {
        initialize();
    }

    public void initialize() {
        try {
            heart = ImageIO.read(getClass().getResourceAsStream("/heart.png"));
        } catch (IOException ex) {
            Logger.getLogger(HUD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void draw(Graphics2D g2d, int hitPoints) {
        for (int i = 0; i < hitPoints; i++) {
            g2d.drawImage(heart, Game.WIDTH - 200 + 50*i, 40, null);
        }
    }
}
