package sk.allas.map;

import java.awt.image.BufferedImage;

/**
 *
 * @author Allas
 */
public class Tile {
    private BufferedImage image;
    private int type;

    public static int SOLID = 0;
    public static int SCENERY = 1;

    public Tile(BufferedImage image, int type) {
        this.image = image;
        this.type = type;
    }

    public BufferedImage getImage() {
        return image;
    }

    public int getType() {
        return type;
    }
}
