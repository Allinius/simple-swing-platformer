package sk.allas.map;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import sk.allas.where.Game;

/**
 *
 * @author Allas
 */
public class LevelMap {
    private Tile[][] tileset;
    private int tilesetRowCount;
    private int[][] tileMap;

    private int tileSize;
    private int rowCount;
    private int colCount;
    private int width;
    private int height;
    private int xMax;
    private int xMin;
    private int yMax;
    private int yMin;

    private double xPos;
    private double yPos;
    private int rowCountOnScreen;
    private int colCountOnScreen;
    private int colDrawStart;
    private int rowDrawStart;

    private double xDest;
    private double yDest;
    private double scrollingSpeed = 0.1;

    public LevelMap(int tileSize, String tilesetName, String map) {
        this.tileSize = tileSize;
        rowCountOnScreen = Game.HEIGHT / tileSize + 1; // check
        colCountOnScreen = Game.WIDTH / tileSize + 1;
        loadTileset(tilesetName);
        loadMap(map);
    }

    private void loadTileset(String imageName) {
        try {
            BufferedImage tilesetImage = ImageIO.read(getClass().getResourceAsStream(imageName));
            tilesetRowCount = tilesetImage.getHeight() / tileSize;
            tileset = new Tile[2][tilesetRowCount];
            BufferedImage subimage;
            for (int i = 0; i < tilesetRowCount; i++) {
                subimage = tilesetImage.getSubimage(0, i * tileSize, tileSize, tileSize);
                tileset[0][i] = new Tile(subimage, Tile.SOLID);

                subimage = tilesetImage.getSubimage(tileSize, i * tileSize, tileSize, tileSize);
                tileset[1][i] = new Tile(subimage, Tile.SCENERY);
            }
        } catch (IOException ex) {
            Logger.getLogger(LevelMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadMap(String map) {
        try {
            InputStream in = getClass().getResourceAsStream(map);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            colCount = Integer.parseInt(br.readLine());
            rowCount = Integer.parseInt(br.readLine());
            width = colCount * tileSize;
            height = rowCount * tileSize;
            xMin = Game.WIDTH / 2;
            yMin = Game.HEIGHT / 2;
            xMax = width - (Game.WIDTH / 2);
            yMax = height - (Game.HEIGHT / 2);

            tileMap = new int[colCount][rowCount];

            String delimiter = "\\s+"; //check
            for (int i = 0; i < rowCount; i++) {
                String line = br.readLine();
                String[] codes = line.split(delimiter);
                for (int j = 0; j < colCount; j++) {
                    tileMap[j][i] = Integer.parseInt(codes[j]);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LevelMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setPosition(int x, int y) {
        xPos = x;
        yPos = y;

        xDest = xPos;
        yDest = yPos;

        clampXY();

        colDrawStart = ((int)xPos - (Game.WIDTH / 2)) / tileSize;
        rowDrawStart = ((int)yPos - (Game.HEIGHT / 2)) / tileSize;
    }

    public void setDestination(int xd, int yd) {
        xDest = xd;
        yDest = yd;
        clampXY();
    }

    public int getTileSize() {
        return tileSize;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getxPos() {
        return (int)xPos;
    }

    public int getyPos() {
        return (int)yPos;
    }

    public int getType(int x, int y) {
        int tempTile = tileMap[x][y];
        return tileset[tempTile % 2][tempTile / 2].getType();
    }

    private void clampXY() {
        if (xPos < xMin) {
            xPos = xMin;
        } else if (xPos > xMax) {
            xPos = xMax;
        }
        if (yPos < yMin) {
            yPos = yMin;
        } else if (yPos > yMax) {
            yPos = yMax;
        }

        if (xDest < xMin) {
            xDest = xMin;
        } else if (xDest > xMax) {
            xDest = xMax;
        }
        if (yDest < yMin) {
            yDest = yMin;
        } else if (yDest > yMax) {
            yDest = yMax;
        }
    }

    public void draw(Graphics2D g2d) {
        for (int row = rowDrawStart; row <= rowDrawStart + rowCountOnScreen; row++){
            if (row >= rowCount) {
                break;
            }
            for (int col = colDrawStart; col <= colDrawStart + colCountOnScreen; col++) {
                if (col >= colCount) {
                    break;
                }
                int code = tileMap[col][row];
                BufferedImage tempImage = tileset[code % 2][code / 2].getImage();
                g2d.drawImage(tempImage, col * tileSize - ((int)xPos - (Game.WIDTH / 2)), row * tileSize - ((int)yPos - (Game.HEIGHT / 2)), null);
            }
        }
    }

    public void update() {
        if (Math.abs(xDest - xPos) < 1) {
            xPos = xDest;
        }
        if (Math.abs(yDest - yPos) < 1) {
            yPos = yDest;
        }
        xPos = xPos + ((xDest - xPos) * scrollingSpeed);
        yPos = yPos + ((yDest - yPos) * scrollingSpeed);

        colDrawStart = ((int)xPos - (Game.WIDTH / 2)) / tileSize;
        rowDrawStart = ((int)yPos - (Game.HEIGHT / 2)) / tileSize;
    }
}
